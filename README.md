## Cloud Monitoring, Diagnostics, Accounting, and Auditing

### Introduction
CMDAA is a multifaceted project developing a cohesive suite of cloud monitoring, diagnostic, accounting, and auditing applications. While there are plenty of applications for collecting and monitoring logs as well as metrics from cloud systems (ELK, Loggly, Splunk, Scalyr, Ganglia, Nagios, Icinga, Sensu, Zabbix, Cloudera Manager, AppDynamic, Micro Focus, Sematext Cloud, Moogsoft, OpsRamp, and Dynatrace), none of them facilitate the parsing of log messages and  few do a comprehensive analysis of logs and metrics for automated failure diagnosis. There is even a lack of applications that ease the development and deployment of configurations to collect the logs and metrics. CMDAA is a project developing a series of applications to fill those voids.

### Monitoring
There are two predominant tenets on how logs and metrics should be collected and analyzed in a cloud system. One is to have a minimal footprint within the components of the system; any process running that collects logs or metrics should use an insignificant amount of resources and quickly offload all collected data to a separate system. The Elastic beats architecture follows this tenet. The second tenet gives precedence to scalability and responsiveness by collecting logs and metrics as well as analyzing them in real time within the same process. The Logstash and Fluentd applications are often used for this tenet. There are pros and cons to each set of objectives. The best tenet for a proposed system depends on its scope. Scalability and responsiveness are key aspects of the CMDAA project, so it follows the second tenet.

### Logs
The complexity of today's distributed applications and computing systems requires that a distributed logging subsystem be an integral component of their architectures. Such systems are used for monitoring, troubleshooting, and debugging. While the use of the [standard severity levels](https://en.wikipedia.org/wiki/Syslog#Severity_level) in log messages enables administrators to create alerts for existing issues, a comprehensive analysis of logs and metrics is often needed to find the root cause of a problem. An exhaustive analysis of all log messages that an application or system can generate is a laborious if not an impossible task. Administrators must know all the log messages a system could generate, then develop, test, and deploy regular expressions to parse them. But administrators rarely have access to all the possible log messages, let alone the resources to create parsers for them. The common workflow for parsing log messages is as follows:
1. Start with a collection of log messages.
2. Manually identify the classes of log messages it contains by the substrings they share.
3. Create parsers to extract the important information from each class of log messages.
4. Deploy the new parsing configurations.
5. Repeat the process when new classes of log messages occur.

This workflow is so resource intensive that administrators can usually only apply it to the most important logs messages of the most important application and system logs. This manually intensive workflow makes it an impractical method to attain a comprehensive analysis of all the log messages of a large application or system. A major component of the CMDAA project is an application called Grokmaker that resolves those problems.

#### Grokmaker
Grokmaker is an application that streamlines the development and deployment of configurations for parsing the log messages of an application or system. It uses Fluentd to ingest and parse log messages, and Elasticsearch and a Timescale database to store and index the parsed messages. It also has a custom web application that facilitates the management, development, and deployment of the configurations. Grokmaker monitors log sources for new classes of log messages and automatically does a statistical analysis of the new messages to create regular expressions to parse them. It notifies users of the new classes of log messages that are not being identified or parsed. It then creates regular expressions to parse them, and enables users to fully vet the new expressions before deploying them. The Grokmaker documentation provides more detail about its design and usage.

A grok is a regular expression that also identifies the fields the extracted data is to be assigned to. For example, the grok %{DATE:date} %{NUMBER:bytes} %{NUMBER:duration} would extract out a date and two numbers. A date is stored into the date field and the two subsequent numbers are stored in the bytes and duration fields respectively.

### Metrics
Collecting the performance measurements of a system or application is crucial for accurately monitoring its health and diagnosing problems. The CMDAA project uses collectd, elastic beats, and fluentd to collect measurements from the processes comprising an application or system. Since such measurements are predominately numerical time series data, they are stored in an Elasticsearch instance or a Timescale database and visualized using Kibana or Prometheus.

#### Key Performance Indicators
Mature applications usually generate the measurements required to manage their performance; but if such measurements are inadequate more must be collected via autonomous processes that measure the various aspects of the application’s performance. In the CMDAA project such measurements are called key performance indicators or canary metrics and they are collected from the entire process stack of a cloud system. The full suite of metrics collected is provided in the documentation.

### Diagnostics
The CMDAA project uses statistical techniques to identify potential anomalies within collected metrics. The statistical methods are used to detect anomalies in real time and retrospectively to help diagnose the cause of system issues.

### Anomaly Detection
#### Streaming Metrics Anomaly Detection
The detection of anomalies from streaming univariant metric sources is performed using the parametric technique called seasonal-hybrid extreme studentized deviate (S-H-ESD).[^1]<sup>,</sup>[^2] It is  implemented in a fluentd plugin that can receive metrics from multiple sources.
[^1]: Hochenbaum, Jordan, Owen S. Vallis, and Arun Kejariwal. "[Automatic anomaly detection in the cloud via statistical learning](https://arxiv.org/pdf/1704.07706.pdf)." arXiv preprint arXiv:1704.07706 (2017)
[^2]: [Introducing practical and robust anomaly detection in a time series](https://blog.twitter.com/engineering/en_us/a/2015/introducing-practical-and-robust-anomaly-detection-in-a-time-series.html), 6 January 2015

#### Log Message Anomaly Detection
A fluentd plugin, called chilogger, coupled with a persistent storage application, uses the log likelihood statistic to determine the probability that a log message is occurring with an unusual frequency[^3]. The statistic is only calculated for log messages that are identified by a grok since the class of a log message is needed to compute the statistic. Chilogger dynamically divides the entire time that a class of log messages has been collected into old and new. The new timeframe is the past n minutes, where n configurable, and the old timeframe is all time preceding the new timeframe. Chilogger calculates the occurrence of all classes of log messages in the new and old time frames in real time and uses the log likelihood statistic to identify any classes of log messages in the new timeframe that are occurring with an unusual frequency compared to their occurrence in the old timeframe. A detailed description of its implementation is included in the chilogger documentation.
[^3]: Dunning, Ted. "[Accurate methods for the statistics of surprise and coincidence](https://scholar.google.com/scholar?q=Accurate+Methods+for+the+Statistics+of+Surprise+and+Coincidence)." Computational linguistics 19.1 (1993): 61-74.

### Analytics
The CMDAA project design has retrospective and streaming analytics to facilitate predicting, detecting, diagnosing, and troubleshooting issues.

#### Retrospective Analysis System
The retrospective analysis system will be Jupyter notebooks with the ability to create and combine existing analytics using any of the metrics and logs collected. It also provides the ability to research, develop, test, deploy streaming analytics, dashboards, and alerts.

#### Streaming Analytics
All of the metrics collected are forwarded to a time series database and an Apache Kafka instance. The metrics in the Kafka instance feed all of the streaming analytics, which in turn feed the Kafka and the time series database.

### Accounting and Auditing
The account and auditing components of the CMDAA project have not been fully vetted. The objective is to calculate all the resources used by each project/process within a Kubernetes cluster. This can be achieved via labels, namespaces, and cgroups. The objective for auditing is to ensure all applications run within the cluster confirm to mandated policies.
